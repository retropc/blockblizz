$url = "https://ip-ranges.amazonaws.com/ip-ranges.json"
$region = "me-south-1"

function Elevate {
  $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
  if (-NOT $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write "I need admin to modify firewall rules, elevating in 3 seconds..."
    Sleep 3
    Start-Process powershell.exe -ArgumentList $elevated_cmd -Verb RunAs
    exit
  }
}

function Delete-Existing {
  Write "deleting existing rule..."
  Remove-NetFirewallRule -Name "block-blizz-me"
  if ($?) {
    Write "done"
  } else {
    Write "error deleting"
  }
  return
}

function Add-Or-Update {
  Write "fetching ips from AWS: $url ..."
  $j = Invoke-WebRequest $url | ConvertFrom-Json

  $ips = @()
  $j.prefixes | ForEach-Object {
    if ($_.region -eq "me-south-1" -And $_.service -eq "EC2") {
      $ips += $_.ip_prefix
    }
  }

  Write "subnets in region ${region}: $ips"

  Get-NetFirewallRule -Name "block-blizz-me" -ErrorAction SilentlyContinue | Out-Null
  if ($?) {
    Write "deleting existing rule..."
    Remove-NetFirewallRule -Name "block-blizz-me" -ErrorAction SilentlyContinue | Out-Null
    if ($?) {
      Write "success!"
    } else {
      Write-Warning "couldn't delete existing rule!"
    }
  }

  Write "adding new firewall rule..."
  New-NetFirewallRule -DisplayName "block blizzard middle eastern servers" -Name block-blizz-me -Direction outbound -Action Block -RemoteAddress $ips -ErrorAction SilentlyContinue | Out-Null
  if ($?) {
    Write "success!"
    Write "region $region should now be blocked (requires game to be relaunched)"
  } else {
    Write-Warning "failure: error occurred -- rule not added"
  }
}

if ($args.Length -eq 0) {
  Write "1. add/update firewall rule to block region $region"
  Write "2. remove previously added rule"

  $mode = Read-Host -Prompt "please choose"
  if ($mode -eq "1") {
  } elseif ($mode -eq "2") {
  } else {
    Write "invalid option"
    exit
  }
  
  $elevated_cmd = "-ExecutionPolicy bypass -NoProfile -File `"" + $myinvocation.mycommand.path + "`" " + $mode
  Elevate
} else {
  $mode = $args[0]
}

if ($mode -eq "1") {
  Add-Or-Update
} elseif ($mode -eq "2") {
  Delete-Existing
}
Pause
